# this document gives an outline of computing the quotient in the
# second bullet point of section 0. GOALS
import csv
import math
from sage.all import *

# def orderCharacter(character, p):
# this function computes the order of a character mod p
#    for exponent in range(1, p):
    # find the order of the character, i.e. the minimum exponent that
    # turns a character into a principal character
 #       outputSet = set()
  #      for i in range(1, p):
   #         result = pow(character[i], exponent)
            # compute the output of character raised to exponent
    #        outputSet.add(result)
     #       if 1 in outputSet and len(outputSet) == 1:
            # if all the outputs of the character raised to the exponent
            # are 1, then we have found the order
      #          return exponent

def quotient(m, p):
# this function computes the quotient in the second bullet point of
# section 0. GOALS with given order m and modulus p
    G = DirichletGroup(p)
    # create the group of characters mod p
    orderList = []
    # create a list for characters of order m
    for character in G:
        if character.order() == m:
            orderList.append(character)
    sum = 0
    for character in orderList:
        for num in range(1, p):
            sum += num * character(num)
    return sum / p

# Some further notes: our aim is to generate the quotients for the first
# 1000 primes p and all m's that are proper factors of p - 1 (which means m is
# less than p - 1).

with open('primes.txt', 'r') as file:
    content = file.read()

primes = eval(content)

def make_dict(plist):
    all_quo = dict()
    for p in plist:
        factors = divisors(p - 1)
        proper_factors = [factor for factor in factors if factor != p - 1]
        p_quo = dict()
        for m in proper_factors:
            p_quo[m] = quotient(m,p)
        all_quo[p] = p_quo
    return all_quo

def find_max(data):
    max_length = 0

    for inner_dict in data.values():
        current_length = len(inner_dict)
        if current_length > max_length:
            max_length = current_length

    return max_length

def dict_to_csv(data, csv_file_path):
    # Get all unique inner keys across the outer dictionaries
    all_inner_keys = set()
    for inner_dict in data.values():
        all_inner_keys.update(inner_dict.keys())
    all_outer_keys = data.keys()
    rows_len = find_max(data)

    # Open the CSV file for writing
    with open(csv_file_path, 'w', newline='') as csv_file:
        csv_writer = csv.writer(csv_file)

        # Write the header row
        header_row = ['Prime']
        for outer_key in all_outer_keys:
            header_row.extend([outer_key, outer_key])
        csv_writer.writerow(header_row)

        # Iterate through the outer dictionary
        for i in range(rows_len):
            row_data = [i]
            for inner_dict in data.values():
                if len(inner_dict) > i:
                    row_data.extend([list(inner_dict.keys())[i], list(inner_dict.values())[i]])
                else:
                    row_data.extend(['', ''])
            csv_writer.writerow(row_data)

    print(f'CSV file "{csv_file_path}" has been generated.')


data = make_dict(primes[:100])
dict_to_csv(data, "data.csv")
