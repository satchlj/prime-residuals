import math

primes = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229}

# dlist = range(4,231,2)
dlist = [2]
def multiplicative_group(p):
    S = set()
    for i in range(1, p):
        if math.gcd(i, p) == 1:
            S.add(i)
    return S

def raise_to_power_mod(S, d, p):
    new_set = set()

    for element in S:
        result = pow(element, d, p)
        new_set.add(result)

    return new_set

def find_sets(plist,dlist):
    sets = {}
    for d in dlist:
        averages = {}
        for p in plist:
            Zpx = multiplicative_group(p)
            S = raise_to_power_mod(Zpx, d, p)
            result = sum(S)/p
            averages[p] = result
        sets[d] = averages
    return sets
import csv

def dict_to_csv(data, filename):
    # Extract the keys from the first inner dictionary
    keys = data[next(iter(data))].keys()

    # Open the CSV file in write mode
    with open(filename, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=keys)

        # Write the header row
        writer.writeheader()

        # Write each row in the CSV file
        for row in data.values():
            writer.writerow(row)

data = find_sets(primes, dlist)
dict_to_csv(data, "data3.csv")
