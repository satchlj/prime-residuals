import math

with open('primes.txt', 'r') as file:
    content = file.read()

primes = eval(content)

with open('classnumbers.txt', 'r') as file:
    content = file.read()

classnumbersdict = eval(content)
classnumbers = list(classnumbersdict.values())

dlist = range(2,231,2)

def multiplicative_group(p):
    S = set()
    for i in range(1, p):
        if math.gcd(i, p) == 1:
            S.add(i)
    return S

def raise_to_power_mod(S, d, p):
    new_set = set()
    for element in S:
        result = pow(element, d, p)
        new_set.add(result)
    return new_set

def maxclass(clist, i):
    maxx = max(clist[:i+1])
    return maxx

def find_sets(plist,dlist):
    sets = {}
    answer = "yes"
    for d in dlist:
        averages = {}
        for i in range(1000):
            Zpx = multiplicative_group(plist[i])
            S = raise_to_power_mod(Zpx, d, plist[i])
            sigma = sum(S)/plist[i]
            fsigma = math.floor(sigma)
            n = math.gcd(plist[i]-1, d)
            result = sigma - ((plist[i]-1)/(2*n))
            if abs(result) > 0.5*maxclass(classnumbers,i):
                answer = "no"
            averages[plist[i]] = result
        sets[d] = averages
    print(answer)
    return sets

data = find_sets(primes, dlist)
