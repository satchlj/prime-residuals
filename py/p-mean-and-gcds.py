import math

with open('primes.txt', 'r') as file:
    content = file.read()

primes = eval(content)

with open('classnumbers.txt', 'r') as file:
    content = file.read()

classnumbersdict = eval(content)
classnumbers = list(classnumbersdict.values())

dlist = range(2,231,2)

def multiplicative_group(p):
    S = set()
    for i in range(1, p):
        if math.gcd(i, p) == 1:
            S.add(i)
    return S

def raise_to_power_mod(S, d, p):
    new_set = set()
    for element in S:
        result = pow(element, d, p)
        new_set.add(result)
    return new_set

def find_sets(plist,dlist,m):
    sets = {}
    for i in range(m):
        diffs = {}
        for d in dlist:
            Zpx = multiplicative_group(plist[i])
            S = raise_to_power_mod(Zpx, d, plist[i])
            sigma = sum(S)/plist[i]
            fsigma = math.floor(sigma)
            n = math.gcd(plist[i]-1, d)
            if plist[i]%4 == 3:
              diff = abs(abs(sigma - ((plist[i]-1)/(2*n))) - classnumbers[i]/2)
            else:
              diff = abs(sigma - ((plist[i]-1)/(2*n)))
            gcd = math.gcd(plist[i]-1, d)
            result = diff
            diffs[gcd] = result
        sets[plist[i]] = diffs
    return sets

import csv

def find_max(data):
    max_length = 0

    for inner_dict in data.values():
        current_length = len(inner_dict)
        if current_length > max_length:
            max_length = current_length

    return max_length

def dict_to_csv(data, csv_file_path):
    # Get all unique inner keys across the outer dictionaries
    all_inner_keys = set()
    for inner_dict in data.values():
        all_inner_keys.update(inner_dict.keys())
    all_outer_keys = data.keys()
    rows_len = find_max(data)

    # Open the CSV file for writing
    with open(csv_file_path, 'w', newline='') as csv_file:
        csv_writer = csv.writer(csv_file)

        # Write the class numbers row
        class_row = ['Class']
        for h in classnumbers[:1000]:
            class_row.extend(['',h])
        csv_writer.writerow(class_row)


        # Write the header row
        header_row = ['Prime']
        for outer_key in all_outer_keys:
            header_row.extend([outer_key, outer_key])
        csv_writer.writerow(header_row)

        # Iterate through the outer dictionary
        for i in range(rows_len):
            row_data = [i]
            for inner_dict in data.values():
                if len(inner_dict) > i:
                    row_data.extend([list(inner_dict.keys())[i], list(inner_dict.values())[i]])
                else:
                    row_data.extend(['', ''])
            csv_writer.writerow(row_data)

    print(f'CSV file "{csv_file_path}" has been generated.')

# set m= number of primes/class numbers, max 100,000
# before running, make sure the appropriate text files are copied to the working directory
data = find_sets(primes, dlist, 0)
dict_to_csv(data, "data.csv")
