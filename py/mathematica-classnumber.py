import math
from wolframclient.evaluation import WolframLanguageSession
from wolframclient.language import wl, wlexpr
session = WolframLanguageSession()

with open('primes.txt', 'r') as file:
    # Step 2: Read the content of the file as a string
    content = file.read()

# Step 3: Convert the string back to a Python list
# Using eval() (only recommended if you trust the source of the file)
primes = eval(content)

# Define the input expression
input_expression = 'NumberFieldClassNumber[Sqrt[-{p}]]'

# Evaluate the expression using Mathematica
results = {}
for prime in primes:
    result = session.evaluate(input_expression.format(p=prime))
    results[prime] = result

print(results)
